import time

from .utils import setup_simulator


def test_start_stop():
    simulator = setup_simulator()
    simulator.run()
    time.sleep(0.1)
    simulator.hold()
    simulator.exit()


def test_hold_run():
    simulator = setup_simulator()
    simulator.run()
    time.sleep(0.1)
    simulator.hold()
    time.sleep(0.1)
    simulator.run()
    time.sleep(0.1)
    simulator.hold()
    simulator.exit()
