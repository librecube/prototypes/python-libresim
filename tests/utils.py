import libresim


def setup_simulator(
    root_models=None,
    time_progress=libresim.SimulationTimeProgress.FREE_RUNNING,
):
    simulator = libresim.Simulator("TestSim")
    simulator.set_time_progress(time_progress)
    if root_models:
        for model in root_models:
            simulator.add_model(model)
    simulator.publish()
    simulator.configure()
    simulator.connect()
    return simulator
