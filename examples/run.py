import argparse
import importlib
from datetime import datetime

import libresim

# load model from external file
parser = argparse.ArgumentParser()
parser.add_argument("modelfile")
args = parser.parse_args()

# allow loading modules from subfolders
modelfile = args.modelfile
modelfile = modelfile.replace("/", ".")  # Linux
modelfile = modelfile.replace("\\", ".")  # Windows
# strip away file ennding if present
if modelfile.endswith(".py"):
    modelfile = modelfile.strip(".py")

module = importlib.import_module(modelfile)

# create simulator
simulator = libresim.Simulator("Simulator")
simulator.set_time_progress(libresim.SimulationTimeProgress.REALTIME)
# simulator.set_time_progress(libresim.SimulationTimeProgress.ACCELERATED, 10)
# simulator.set_time_progress(libresim.SimulationTimeProgress.FREE_RUNNING)

# add models and services
for component in module.root:
    if isinstance(component, libresim.Model):
        simulator.add_model(component)
    elif isinstance(component, libresim.Service):
        simulator.add_service(component)

print("Simulator tree:")
libresim.print_simulator_tree(simulator)
print()

simulator.publish()

print("Published fields:")
libresim.print_published_fields(simulator)
print()

simulator.configure()
simulator.connect()

# set epoch time to now
simulator.get_time_keeper().set_epoch_time(datetime.utcnow())

# simulator.restore("breakpoint.json")

print("Simulation running...")
print("Press <Ctr-C> to stop")
print()
simulator.run()

try:
    from libresim.dataserver import DataServer

    dataserver = DataServer(simulator)
    dataserver.run()

except KeyboardInterrupt:
    pass

simulator.hold()

# simulator.store("breakpoint.json")

simulator.exit()
