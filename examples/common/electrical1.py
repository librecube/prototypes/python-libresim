from libresim import Model, EntryPoint, EntryPointPublisher
from libresim.lib.common.electrical import (
    ElectricalNetwork,
    create_power_source,
    create_terminator,
    create_node,
)

# source1 ----\    /---- terminator1
#              node1
# source2 ----/    \---- terminator2

electrical_network = ElectricalNetwork("ElectricalNetwork")
source1 = create_power_source("Source1", voltage=5, active=1)
source2 = create_power_source("Source2", voltage=3, active=1)
terminator1 = create_terminator(
    "Terminator1", intrinsic_load=2, low_voltage_threshold=2
)
terminator2 = create_terminator(
    "Terminator2", intrinsic_load=3, low_voltage_threshold=6
)
node1 = create_node("Node1", intrinsic_load=1, low_voltage_threshold=2)

electrical_network.connect_elements(node1, source1)
electrical_network.connect_elements(node1, source2)
electrical_network.connect_elements(terminator1, node1)
electrical_network.connect_elements(terminator2, node1)


class Example(Model, EntryPointPublisher):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)

        self.add_entry_point(
            EntryPoint("print_output", "", self, function=self.print_output)
        )

    def connect(self):
        self.get_scheduler().add_immediate_event(
            self.get_entry_point("print_output")
        )
        # need to call this to propagate values up- and downstream
        electrical_network.do_update()

    def print_output(self):
        for x in [source1, source2, node1, terminator1, terminator2]:
            print(
                f"{x.get_name()}: "
                + f"{x.voltage.get_value()}[V], "
                + f"{x.load.get_value()}[W], "
            )
        print()


root = [Example("Example"), electrical_network]
