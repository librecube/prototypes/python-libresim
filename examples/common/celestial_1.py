from datetime import datetime

from libresim import Model, EntryPoint, EntryPointPublisher
from libresim.lib.common import CoordinateSystem, CoordinateSystemTree
from libresim.lib.common.celestial import Earth  # Jupiter, Mars


cst = CoordinateSystemTree()

# Earth

ece = CoordinateSystem("EarthCentricEcliptical")
cst.add_coordinate_system(ece)
ecee = CoordinateSystem("EarthCentricEarthEquatorial")
cst.add_coordinate_system(ecee)
ecef = CoordinateSystem("EarthCentricEarthFixed")
cst.add_coordinate_system(ecef)

earth = Earth()
earth.set_earth_centric_ecliptical(ece)
earth.set_earth_centric_earth_equatorial(ecee)
earth.set_earth_centric_earth_fixed(ecef)

# Jupiter

# jce = CoordinateSystem("JupiterCentricEcliptical")
# cst.add_coordinate_system(jce)

# jupiter = Jupiter()
# jupiter.set_jupiter_centric_ecliptical(jce)

# Mars

# mce = CoordinateSystem("MarsCentricEcliptical")
# cst.add_coordinate_system(mce)

# mars = Mars()
# mars.set_mars_centric_ecliptical(mce)


class Example(Model, EntryPointPublisher):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)

        self.add_entry_point(
            EntryPoint("update", "", self, function=earth.do_update)
        )
        # self.add_entry_point(
        #     EntryPoint("update", "", self, function=jupiter.do_update)
        # )
        # self.add_entry_point(
        #     EntryPoint("update", "", self, function=mars.do_update)
        # )

    def connect(self):
        self.get_time_keeper().set_epoch_time(datetime(2023, 4, 5, 20))

        self.get_scheduler().add_simulation_time_event(
            earth.get_entry_point("update"),
            simulation_time=0,
            cycle_time=1,
            repeat=-1,
        )
        # self._scheduler.add_simulation_time_event(
        #     jupiter.get_entry_point("update"),
        #     simulation_time=0,
        #     cycle_time=1,
        #     repeat=-1,
        # )
        # self._scheduler.add_simulation_time_event(
        #     mars.get_entry_point("update"),
        #     simulation_time=0,
        #     cycle_time=1,
        #     repeat=-1,
        # )

    def print_fields(self):
        EarthCoordSysGeoEq = self._resolver.resolve_absolute("Earth")
        print(
            EarthCoordSysGeoEq.get_name(),
            EarthCoordSysGeoEq.position.x.get_value(),
            EarthCoordSysGeoEq.position.y.get_value(),
            EarthCoordSysGeoEq.position.z.get_value(),
        )


root = [cst, earth, Example("Example")]
