from libresim.lib.common import CoordinateSystem, CoordinateSystemTree


coordsys1 = CoordinateSystem("CoordSys1")
coordsys1.set_position(1, 2, 3)
coordsys1.set_rotation(0, 0, 0, 1)

coordsys2 = CoordinateSystem("CoordSys2")
coordsys2.set_parent_coordinate_system("CoordSys1")
coordsys2.set_position(4, 5, 6)
coordsys2.set_rotation(1, 0, 0, 0)

# Optionally register them with coordinate system service
coordinate_system_tree = CoordinateSystemTree()
coordinate_system_tree.add_coordinate_system(coordsys1)
coordinate_system_tree.add_coordinate_system(coordsys2)

root = [coordinate_system_tree]
