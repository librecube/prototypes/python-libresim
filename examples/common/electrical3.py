from libresim import Model, EntryPoint, EntryPointPublisher
from libresim.lib.common.electrical import (
    create_electrical_network_from_file,
)

# source1 ----\    /---- terminator1
#              node1
# source2 ----/    \---- terminator2


electrical_network = create_electrical_network_from_file(
    "config/electrical_config.yaml"
)


class Example(Model, EntryPointPublisher):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)

        self.add_entry_point(
            EntryPoint("print_output", "", self, function=self.print_output)
        )

    def connect(self):
        self._scheduler.add_simulation_time_event(
            self.get_entry_point("print_output"),
            simulation_time=0,
            cycle_time=1,
            repeat=-1,
        )
        # need to call this to propagate values up- and downstream
        electrical_network.do_update()

    def print_output(self):
        elements = self._resolver.resolve_absolute(
            "Models/ElectricalNetwork/Elements"
        )
        for x in elements.get_components():
            print(
                f"{x.get_name()}: "
                + f"{x.voltage.get_value()}[V], "
                + f"{x.load.get_value()}[W], "
            )
        print()


root = [Example("Example"), electrical_network]
