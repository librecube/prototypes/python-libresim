from libresim import Model, SimpleField, EntryPoint, EntryPointPublisher


class Counter(Model, EntryPointPublisher):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)

        self.count = SimpleField("count", "Holds the counter value", self)

        self.add_entry_point(
            EntryPoint("increase", "Inc counter", self, function=self.do_count)
        )
        self.add_entry_point(
            EntryPoint("reset", "Reset counter", self, function=self.do_reset)
        )

    def publish(self):
        self.get_receiver().publish_field(self.count)
        self.get_receiver().publish_operation(self.do_count, "Inc counter")

    def configure(self):
        self.count.set_value(0)

    def connect(self):
        self.get_scheduler().add_simulation_time_event(
            self.get_entry_point("increase"),
            simulation_time=0,
            cycle_time=1,
            repeat=-1,
        )
        self.get_scheduler().add_simulation_time_event(
            self.get_entry_point("reset"), simulation_time=4
        )
        self.get_logger().log_debug(self, "Counter Model is now connected")

    def do_reset(self):
        self.count.set_value(0)

    def do_count(self):
        self.get_logger().log_info(
            self, f"count is now {self.count.get_value()}"
        )
        self.count.set_value(self.count.get_value() + 1)


root = [Counter("Counter", "A simple counter")]
