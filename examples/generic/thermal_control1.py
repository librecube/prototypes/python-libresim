"""
In this example a basic temperature control is implemented that switches
a heater on and off to keep the temperature within defined limits.

    PowerSource ---- PowerSwitch ---- Heater (= Switch + Terminator)

"""

from libresim import (
    Model,
    EntryPoint,
    EntryPointPublisher,
)
from libresim.lib.common.thermal import (
    ThermalNetwork,
    create_hot_object,
    create_thermal_node,
    create_related_hot_object,
)

from libresim.lib.common.electrical import (
    ElectricalNetwork,
    create_power_source,
    create_switch,
    create_terminator,
)

from libresim.lib.generic.electrical_power import LatchedSwitch
from libresim.lib.generic.thermal_control import Heater, Thermistor


# thermal configuration

thermal_network = ThermalNetwork("ThermalNetwork")
ho_heater = create_hot_object("HO_Heater", status=1)
tn_heater = create_thermal_node(
    "TN_Heater",
    base_temperature=20,
    current_temperature=20,
    rise_rate=2,
    fall_rate=1,
)
tn_heater.add_related_hot_object(
    create_related_hot_object(
        "TN_Heater", hot_object_name="HO_Heater", maximum_effect=20
    )
)
thermal_network.add_hot_object(ho_heater)
thermal_network.add_thermal_node(tn_heater)


# electrical configuration

electrical_network = ElectricalNetwork("ElectricalNetwork")
source = create_power_source("Source", voltage=5, active=1)
switch = create_switch("Switch", intrinsic_load=0.1, position=1)
terminator = create_terminator(
    "Terminator", intrinsic_load=2, low_voltage_threshold=2
)
electrical_network.connect_elements(switch, source)
electrical_network.connect_elements(terminator, switch)


# generic units

heater = Heater("Heater")
heater.set_hot_object(ho_heater)
heater.set_electrical_terminator(terminator)
latched_switch = LatchedSwitch("LatchedSwitch")
latched_switch.set_electrical_switch(switch)
thermistor = Thermistor("Thermistor")
thermistor.set_thermal_node(tn_heater)


class Example(Model, EntryPointPublisher):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)

        self.add_entry_point(
            EntryPoint("print_output", "", self, function=self.print_output)
        )

    def connect(self):
        self._scheduler.add_simulation_time_event(
            self.get_entry_point("print_output"),
            simulation_time=0,
            cycle_time=1,
            repeat=-1,
        )

    def print_output(self):
        print(heater.is_powered())
        print(ho_heater.status.get_value())
        print(thermistor.get_temperature())

        for x in [source, switch, terminator]:
            print(
                f"{x.get_name()}: "
                + f"{x.voltage.get_value()}[V], "
                + f"{x.load.get_value()}[W], "
            )
        print()

        if thermistor.get_temperature() > 30:
            latched_switch.do_open()
        elif thermistor.get_temperature() < 25:
            latched_switch.do_close()


root = [Example("Example"), thermal_network, electrical_network, heater]
