from libresim import (
    Model,
    EntryPoint,
    EntryPointPublisher,
)
from libresim.lib.common.thermal import (
    ThermalNetwork,
    create_hot_object,
    create_thermal_node,
    create_related_hot_object,
)

from libresim.lib.common.electrical import (
    ElectricalNetwork,
    create_power_source,
    create_switch,
    create_terminator,
)

from libresim.lib.common.parameter import Parameter, ParameterPool
from libresim.lib.generic.thermal_control import Heater, Thermistor


# electrical network
electrical_network = ElectricalNetwork("ElectricalNetwork")
source = create_power_source("Source", voltage=28, active=1)
switch1 = create_switch("Switch1", intrinsic_load=0, position=1)
switch2 = create_switch("Switch2", intrinsic_load=0, position=1)
terminator = create_terminator(
    "Terminator", intrinsic_load=22, low_voltage_threshold=0
)
electrical_network.connect_elements(switch1, source)
electrical_network.connect_elements(switch2, switch1)
electrical_network.connect_elements(terminator, switch2)


# thermal configuration
thermal_network = ThermalNetwork("ThermalNetwork")
ho_heater = create_hot_object("HO_Heater", status=1)
tn_thermistor = create_thermal_node(
    "TN_Heater",
    base_temperature=20,
    current_temperature=20,
    rise_rate=2,
    fall_rate=1,
)
tn_thermistor.add_related_hot_object(
    create_related_hot_object(
        "TN_Heater", hot_object_name="HO_Heater", maximum_effect=20
    )
)
thermal_network.add_hot_object(ho_heater)
thermal_network.add_thermal_node(tn_thermistor)


# generic units
heater = Heater("Heater")
heater.set_hot_object(ho_heater)
heater.set_electrical_switch(switch2)
heater.set_electrical_terminator(terminator)
thermistor = Thermistor("Thermistor")
thermistor.set_thermal_node(tn_thermistor)

# parameters
parameter_pool = ParameterPool("ParameterPool")
param1 = Parameter("Param1")
param2 = Parameter("Param2")
parameter_pool.add_parameter(param1)
parameter_pool.add_parameter(param2)

heater.get_model_parameter("switch_status").connect_to_write_parameter(param1)


class Example(Model, EntryPointPublisher):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)

        self.add_entry_point(
            EntryPoint("print_output", "", self, function=self.print_output)
        )

    def connect(self):
        self._scheduler.add_simulation_time_event(
            thermistor.get_entry_point("update"),
            simulation_time=0,
            cycle_time=1,
            repeat=-1,
        )
        self._scheduler.add_simulation_time_event(
            self.get_entry_point("print_output"),
            simulation_time=0,
            cycle_time=1,
            repeat=-1,
        )

    def print_output(self):
        heater_status = heater.get_model_parameter("switch_status").get_value()
        temperature = thermistor.get_model_parameter(
            "measured_temperature"
        ).get_value()
        print(heater_status)
        print(temperature)

        for x in [source, switch1, switch2, terminator]:
            print(
                f"{x.get_name()}: "
                + f"{x.voltage.get_value()}[V], "
                + f"{x.load.get_value()}[W], "
            )
        print()

        if temperature > 30:
            heater.do_switch_off()
        elif temperature < 25:
            heater.do_switch_on()


root = [
    Example("Example"),
    thermal_network,
    electrical_network,
    heater,
    thermistor,
    parameter_pool,
]
