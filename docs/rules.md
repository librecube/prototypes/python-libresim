# General Rules

## Model elements names

Mode failures must end with _failure.
Events must end with _event.
Entrypoints must end with _entrypoints.

When event is assiciated to entrypoint it shall have same name: update_event
-> update_entrypoint.

```plantuml
Bob -> Alice : hello
Alice -> Bob : hi
```
