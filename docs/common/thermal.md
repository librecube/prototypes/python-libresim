# Common Models - Thermal

The thermal model library provides the means to simulate simplified thermal
networks, composed of heat sources (hot objects) and heat sinks (thermal nodes).
Both are contained in an encompassing thermal network.

![](figures/thermal_network_hierarchy.png)

Hot objects typically represent heaters, powered units or hot surfaces, which
generate heat. Thermal nodes typically represent thermistors, which measure
temperature.

The temperature of a thermal nodes is influenced by the contribution of
a number of related hot objects, that is, the thermal node holds a list of
related hot objects. In this list, each hot object contributes with a specified
amount of temperature.

The computation of thermal node temperature is as follows:

- The thermal node check the status ( float number) of each related hot object.
- For each related hot object, it multiplies the status with default temperature
contribution to obtain actual temperature contribution.
- The thermal node then computes steady state temperature as sum of all actual
temperature contributions to its base temperature.
- If the computed steady state temperature is over its current temperature then
the thermal node increases its current temperature according to the specified rise rate.
- If the computed steady state temperature is under its current temperature then
the thermal node decreases its current temperature according to the specified fall rate.

## Thermal Configuration File

The configuration of the thermal network can also be defined in a YAML file and
be loaded by the model. The file must be structured as follows.

```yaml
hot_objects:
    HO_1:
        description: mydescription  # description is optional
        status: 0  # status defaults to zero if not provided
    HO_2:
        status: 1
    HO_3:
        status: 0.5

thermal_nodes:
    TN_1:
        description: mydescription  # description is optional
        base_temperature: -37.0
        current_temperature: 22.0
        rise_rate: 0.1
        fall_rate: 0.05
        related_hot_objects:
            HO_1: 20
    TN_2:
        base_temperature: 0.0
        current_temperature: 23.0
        rise_rate: 0.2
        fall_rate: 0.01
        related_hot_objects:
            HO_2: 30
            HO_3: 40       
```
