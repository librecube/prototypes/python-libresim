# LibreSim Common Models Documentation

The generic models comprise a suite of generic simulation models which provide a
basis for other, more specialized models.

The generic models consists of following packages:
- thermal: simplified models for heat generation and temperature measurement
- electrical: models electrical power distribution networks
- orbit: models for spacecraft trajectories and the space environment
- payload: generic components to build payload models
- dynamics: rigid body models for spacecraft attitude and rotations
- datalink: provides functionalities to encode/decode frames and packets
- spacelink: modelling of the radio frequency communication between ground and spacecraft

![](assets/architecture.png)

### Thermal

The thermal module allows to assembly a simplified thermal simulation model. It
consists of Hot Objects and Thermal Nodes. A thermal node can have a number of
related hot objects that influence its temperature. Typically hot objects
represent heaters (or any kind of powered unit that produces heat) and thermal nodes
represent thermistors that measure temperature. Hot objects can be either on or off. The status can be set.

![](assets/generic_model_thermal_architecture.png)

Thermal nodes maintain a current temperature, which goes towards its base
temperature (with a rate given by its fall rate) when the related hot objects
are off. If related hot objects are on, the target temperature is the base
temperature plus the contributions of the active hot objects.

Temperatures are cyclically computed in correspondence to each Thermal Node according to the following algorithm: at each step a steady state temperature is computed as sum of a base temperature, typical of the node, and the temperature contribution of each Related Hot Object whose status is on. If the computed steady state temperature is over the temperature of the previous step then the node temperature is increased with a specified rate, otherwise if the computed steady state temperature is under the temperature of the previous step then the node temperature is decreased with a specified rate.


### Electrical

The electrical module allows to assembly a network of elements to model the
distribution of power within an electrical network. The main elements in such
network are nodes and poles, both derived from a basic element model. Nodes are
mainly characterized by their intrinsic load, whereas poles are characterized
by their switch position.

The following nodes shall be modelled:
- power source: supplies the network and most be the top element
- power node: a point or junction in the network
- power bus: distributes power through a network
- terminator: end point of a branch in the network

The following poles shall be modelled:
- simple pole: can be either open or closed
- fuse: derived from simple pole; it opens when load exceeds a threshold
- multi-throw pole: a pole with a number of switch positions
- transfer switch: the reverse of a multi-throw; selects between power supplies

The electrical network is a collection of those nodes and poles with connections
between those elements. For each element, a number of conditions are defined
which can trigger actions. An action can be used, triggered, or called from other simulation models.

### Orbit

The orbit module provides means to model orbital/trajectory movement of an object
in the solar system and includes the calculation of planet positions, perturbation
effects on the orbit. It computes the position and velocity of the object,
taking into account the forces applied. It also defines a number of generic
coordinate systems, including one for each planet and a local one for the
object (spacecraft) to be modelled.

The following aspects shall be modelled:
- orbit propagator
- celestial body (planets)
- earth magnetic field
- earth atmosphere
- earth albedo
- solar flux
- eclipse
- air drag perturbation
- solar pressure perturbation
- gravity field perturbation
- external perturbation

### Payload

The payload modules provides generic models to allow modelling of typical
packet-based payloads (instruments). It consists of four major components:

- interface to data handling system: deals with payload specific communication protocol, to extract telecommand packets and send out telemetry packets
- payload functional model: implements specific behaviour of payload
- packet encoder: encodes telemetry packets, using data received from functional model
- packet decoder: decodes telecommand packets and forwards to functional model for execution

## Generic Units
