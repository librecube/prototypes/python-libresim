import xmlrpc.client

import click


client = xmlrpc.client.ServerProxy("http://localhost:9999", allow_none=True)


@click.group()
def cli():
    pass


@cli.command()
def ping():
    click.echo(client.ping())


@cli.command()
def setup():
    client.setup()


@cli.command()
def run():
    client.run()


@cli.command()
def hold():
    client.hold()


@cli.command()
def exit():
    client.exit()
