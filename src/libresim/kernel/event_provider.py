from .component import Component


class EventProvider(Component):
    def __init__(self, name, description="", parent=None):
        Component.__init__(self, name, description, parent)
        self._event_sources = []

    def get_event_sources(self):
        return self._event_sources

    def get_event_source(self, name):
        for event_source in self._event_sources:
            if event_source.get_name() == name:
                return event_source

    def add_event_source(self, event_source):
        self._event_sources.append(event_source)

    def clear(self):
        self._event_sources = []
