from .object import Object


class Composite(Object):
    def __init__(self, name, description="", parent=None):
        Object.__init__(self, name, description, parent)
        self._containers = []

    def get_container(self, name):
        for container in self._containers:
            if container.get_name() == name:
                return container

    def get_containers(self):
        return self._containers

    def add_container(self, container):
        container.set_parent(self)
        self._containers.append(container)
