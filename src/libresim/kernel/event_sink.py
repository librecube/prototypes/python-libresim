from .object import Object


class EventSink(Object):
    def notify(self, sender, args=None):
        """This event handler method is called when an event is emitted."""
        raise NotImplementedError
