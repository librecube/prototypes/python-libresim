from ..service import Service
from ..composite import Composite
from ..container import Container


class Resolver(Service):
    def __init__(self, name, description="", parent=None):
        Service.__init__(self, name, description, parent)

    def resolve_absolute(self, absolute_path):
        if not absolute_path.startswith("/"):
            absolute_path = "/" + absolute_path

        if not absolute_path.startswith("/Simulator"):
            absolute_path = "/Simulator" + absolute_path

        absolute_path = absolute_path.strip("/")
        absolute_path = absolute_path.split("/")

        def traverse_tree(component, path):
            if len(path) == 1:
                if component.get_name() == path[0]:
                    return component
                else:
                    return None
            else:
                if isinstance(component, Composite):
                    container = component.get_container(path[1])
                    if container:
                        return traverse_tree(container, path[1:])
                    else:
                        return None
                elif isinstance(component, Container):
                    component = component.get_component(path[1])
                    if component:
                        return traverse_tree(component, path[1:])
                    else:
                        return None

        root_component = self._simulator
        component = traverse_tree(root_component, absolute_path)
        return component

    def resolve_relative(self, relative_path, sender):
        raise NotImplementedError

    def get_absolute_path(self, component):
        path = []

        def go_upwards(component):
            if component.get_parent():
                path.append(component.get_name())
                go_upwards(component.get_parent())

        go_upwards(component)
        path = "/".join(path[::-1])
        return path

    def get_component_by_name(self, name, root=None):
        if root is None:
            root = self.get_simulator()

        def search_model_tree(name, component):
            if component.get_name() == name:
                return component
            if isinstance(component, Container):
                for component_ in component.get_components():
                    result = search_model_tree(name, component_)
                    if result:
                        return result
            elif isinstance(component, Composite):
                for container in component.get_containers():
                    for component_ in container.get_components():
                        result = search_model_tree(name, component_)
                        if result:
                            return result

        return search_model_tree(name, root)
