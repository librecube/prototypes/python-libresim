from .persist import Persist


class Failure(Persist):
    def __init__(self, name, description="", parent=None):
        Persist.__init__(self, name, description, parent)
        self._failed = False

    def fail(self):
        self._failed = True

    def unfail(self):
        self._failed = False

    def is_failed(self):
        return self._failed
