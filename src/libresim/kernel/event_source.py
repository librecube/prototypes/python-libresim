from .object import Object


class EventSource(Object):
    def __init__(self, name, description="", parent=None):
        Object.__init__(self, name, description, parent)
        self._event_sinks = []

    def subscribe(self, event_sink):
        """Subscribe to the event source, i.e. request notifications."""
        self._event_sinks.append(event_sink)

    def unsubscribe(self, event_sink):
        """Unsubscribe from the event source, i.e. cancel notifications."""
        self._event_sinks.pop(event_sink)

    def emit(self, args=None):
        """Emit the events, i.e. call all subscribed notifications."""
        for event_sink in self._event_sinks:
            event_sink.notify(self, args)
