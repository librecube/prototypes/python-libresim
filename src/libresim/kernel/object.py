class Object:
    def __init__(self, name, description="", parent=None):
        self._name = name
        self._description = description
        self._parent = parent

    def get_name(self):
        return self._name

    def get_description(self):
        return self._description

    def set_description(self, description):
        self._description = description

    def get_parent(self):
        return self._parent

    def set_parent(self, parent):
        self._parent = parent

    def get_root(self):
        parent = self.get_parent()
        if parent is None:
            return self
        else:
            return parent.get_root()
