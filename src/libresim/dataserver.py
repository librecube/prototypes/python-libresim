from flask import Flask

import libresim


class DataServer:
    def __init__(self, simulator):
        self._simulator = simulator

    def run(self):
        app = Flask(__name__)

        @app.route("/")
        def home():
            text = (
                "Simulator is running. </br>Go to:</br>"
                "/tree : to see the simulation tree</br>"
                "/data : to see live updates of simulation data</br>"
            )
            return text

        @app.route("/tree")
        def tree():
            return get_simulation_tree(self._simulator)

        @app.route("/data")
        def data():
            return get_simulation_data(self._simulator)

        return app.run(debug=False)


def get_simulation_tree(simulator):
    tree = {}

    def traverse_model_tree(tree, component):
        tree[component.get_name()] = {}

        def traverse_fields(fields):
            for field in fields:
                if isinstance(field, libresim.SimpleField):
                    tree[component.get_name()]["╚═" + field.get_name()] = {}
                elif isinstance(field, libresim.StructureField):
                    traverse_fields(field.get_fields())

        fields = simulator.get_publication().get_fields_of_component(component)
        traverse_fields(fields)

        if isinstance(component, libresim.Container):
            for component_ in component.get_components():
                traverse_model_tree(tree[component.get_name()], component_)
        elif isinstance(component, libresim.Composite):
            for container in component.get_containers():
                traverse_model_tree(tree[component.get_name()], container)

    traverse_model_tree(tree, simulator)

    return tree


def get_simulation_data(simulator):
    time_keeper = simulator.get_time_keeper()
    scheduler = simulator.get_scheduler()
    resolver = simulator.get_resolver()

    scheduled_events = {}

    for event_id, event in scheduler._scheduled_events.items():

        scheduled_events[event_id] = {
            "entry_point": {
                "name": event["entry_point"].get_name(),
                "description": event["entry_point"].get_description(),
                "parent": str(
                    resolver.get_absolute_path(
                        event["entry_point"].get_parent()
                    )
                ),
                "function": str(event["entry_point"]._function.__name__),
            },
            "simulation_time": event["simulation_time"],
            "cycle_time": event["cycle_time"],
            "repeat": event["repeat"],
        }

    vector = {
        "TimeKeeper": {
            "epoch_time": str(time_keeper._epoch_time),
            "mission_start_time": str(time_keeper._mission_start_time),
            "simulation_time": time_keeper._simulation_time,
        },
        "Scheduler": {
            "scheduled_events": scheduled_events,
            # "immediate_events": scheduler._immediate_events,
        },
        "Models": libresim.get_dict_of_field_values(
            simulator.get_resolver(), simulator.get_publication().get_fields()
        ),
    }

    return vector
