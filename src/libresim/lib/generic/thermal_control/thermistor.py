from yaml import safe_load, YAMLError

from libresim import Reference

from libresim.lib.common import GenericModel


def create_thermistor_models_from_file(filename, thermal_network=None):
    with open(filename) as file:
        try:
            config = safe_load(file)
        except YAMLError as error:
            print(error)

    models = []
    for name, attributes in config.items():
        if attributes is None:
            attributes = {}

        description = attributes.get("description", "")
        thermal_node_name = attributes.get("thermal_node")
        thermistor = Thermistor(name, description)

        if thermal_network and thermal_node_name:
            thermal_node = thermal_network.get_thermal_node(thermal_node_name)
            thermistor.set_thermal_node(thermal_node)

        models.append(thermistor)
    return models


class Thermistor(GenericModel):
    def __init__(self, name, description="", parent=None):
        GenericModel.__init__(self, name, description, parent)

        self.add_reference(Reference("ThermalNode"))

    def set_thermal_node(self, component):
        if self.get_reference("ThermalNode").get_count() > 0:
            raise Exception
        self.get_reference("ThermalNode").add_component(component)

    def get_thermal_node(self):
        if self.get_reference("ThermalNode").get_count() > 0:
            return self.get_reference("ThermalNode").get_components()[0]
        else:
            return None

    def get_temperature(self):
        thermal_node = self.get_thermal_node()
        if thermal_node:
            return thermal_node.current_temperature.get_value()
