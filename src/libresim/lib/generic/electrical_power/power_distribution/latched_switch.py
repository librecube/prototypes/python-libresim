from libresim import Reference, SimpleField

from libresim.lib.common.electrical import PositionEnum

from libresim.lib.common import GenericModel


class LatchedSwitch(GenericModel):
    def __init__(self, name, description="", parent=None):
        GenericModel.__init__(self, name, description, parent)

        self.add_reference(Reference("ElectricalSwitch"))

        self.status = SimpleField(
            "status", "Whether the switch is open or closed", self
        )

    def publish(self):
        self._receiver.publish_field(self.status)

    def set_electrical_switch(self, component):
        if self.get_reference("ElectricalSwitch").get_count() > 0:
            raise Exception
        self.get_reference("ElectricalSwitch").add_component(component)

    def get_electrical_switch(self):
        if self.get_reference("ElectricalSwitch").get_count() > 0:
            return self.get_reference("ElectricalSwitch").get_components()[0]
        else:
            return None

    def do_close(self):
        switch = self.get_electrical_switch()
        if switch:
            switch.do_close()
        self.status.set_value(PositionEnum.CLOSED)

    def do_open(self):
        switch = self.get_electrical_switch()
        if switch:
            switch.do_open()
        self.status.set_value(PositionEnum.OPEN)

    def is_closed(self):
        switch = self.get_electrical_switch()
        if switch:
            return switch.is_closed()

    def is_open(self):
        return not self.is_closed()
