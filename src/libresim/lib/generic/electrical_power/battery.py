from libresim import HotObject, Reference, SimpleField
from libresim.lib.common import GenericModel


class Battery(GenericModel):
    def __init__(self, name, description="", parent=None):
        GenericModel.__init__(self, name, description, parent)

        self.add_reference(Reference("HotObject"))
        self.add_reference(Reference("ElectricalTerminator"))

        self.capacity = SimpleField(
            "capacity", "Capacity of the battery", self
        )


class BatteryString(GenericModel):
    # TODO
    pass


class BatteryCell(GenericModel):
    # TODO
    pass
