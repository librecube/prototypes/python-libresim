from libresim import Model, Aggregate, Reference, SimpleField


class ModelParameter(Model, Aggregate):
    """Parameter of a generic model.

    The model parameter shall be used (instead of Field) when its value can
    be sent via telemetry (TM) or received by telecommand (TC).

    When a parameter is linked to a TM parameter in a parameter pool, it
    shall update the value of the associated TM parameter.
    When a parameter is linked to a TC parameter in a parameter pool, its
    value shall be obtained from the associated TC parameter.

    """

    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        Aggregate.__init__(self, name, description, parent)

        self.add_reference(Reference("PoolParameter"))

        self.parameter_value = SimpleField(
            "parameter_value", "Value of the model parameter", self
        )

    def publish(self):
        self._receiver.publish_field(self.parameter_value)

    def set_value(self, value):
        parameters = self.get_reference("PoolParameter")
        for parameter in parameters.get_components():
            parameter.set_value(value)
        self.parameter_value.set_value(value)

    def get_value(self):
        parameters = self.get_reference("PoolParameter")
        if parameters.get_count() > 0:
            parameter = parameters.get_components()[0]
            self.parameter_value.set_value(parameter.get_value())
        return self.parameter_value.get_value()

    def connect_to_pool_parameter(self, parameter):
        self.get_reference("PoolParameter").add_component(parameter)
