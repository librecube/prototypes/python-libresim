from yaml import safe_load, YAMLError

from libresim import Model, Composite, Container, SimpleField
from .quaternion import Quaternion
from .vector import Vector


def create_coordinate_system_tree_from_file(
    filename,
    name="CoordinateSystemTree",
    description="",
    parent=None,
):
    with open(filename) as file:
        try:
            config = safe_load(file)
        except YAMLError as error:
            print(error)
    coordinate_system_tree = CoordinateSystemTree(name, description, parent)
    coordinate_systems = coordinate_system_tree.get_container(
        "CoordinateSystems"
    )

    def parse_data(key, value):
        for name, attributes in value.items():
            if attributes is None:
                attributes = {}
            if key == "coordinate_systems":
                description = attributes.get("description", "")
                coordinate_system = CoordinateSystem(name, description)
                xyz = attributes.get("position")
                if xyz:
                    coordinate_system.position.x.set_value(xyz[0])
                    coordinate_system.position.y.set_value(xyz[1])
                    coordinate_system.position.z.set_value(xyz[2])
                quaternion = attributes.get("rotation")
                if quaternion:
                    coordinate_system.rotation.q1.set_value(quaternion[0])
                    coordinate_system.rotation.q2.set_value(quaternion[1])
                    coordinate_system.rotation.q3.set_value(quaternion[2])
                    coordinate_system.rotation.q4.set_value(quaternion[3])
                coordinate_system.parent_coordinate_system.set_value(
                    attributes.get("parent")
                )
                coordinate_systems.add_component(coordinate_system)

    for key, value in config.items():
        parse_data(key, value)
    return coordinate_system_tree


class CoordinateSystemTree(Model, Composite):
    def __init__(
        self, name="CoordinateSystemTree", description="", parent=None
    ):
        Model.__init__(self, name, description, parent)
        Composite.__init__(self, name, description, parent)

        self.add_container(Container("CoordinateSystems"))

    def add_coordinate_system(self, coordinate_system):
        self.get_container("CoordinateSystems").add_component(
            coordinate_system
        )

    def get_coordinate_system(self, name):
        return self.get_container("CoordinateSystems").get_component(name)


class CoordinateSystem(Model):
    """
    A coordinate system holds the position and the attitude of a body
    relative to the parent coordinate system. If parent is set not defined
    (ie. None) then it is assumed that parent is SunCentricEcliptic.
    A coordinate system can either be static - its position and attitude
    relative to its parent never change - or dynamic. The update of the
    coordinate system position and rotation must be done by other models,
    typically the model that owns the coordinate system.

    """

    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)

        self.parent_coordinate_system = SimpleField(
            "parent_coordinate_system", "Parent coordinate system", self
        )
        self.position = Vector(
            "position", "Position relative to parent coordinate system", self
        )
        self.rotation = Quaternion(
            "rotation", "Rotation relative to parent coordinate system", self
        )

    def publish(self):
        self._receiver.publish_field(self.parent_coordinate_system)
        self._receiver.publish_field(self.position)
        self._receiver.publish_field(self.rotation)

    def set_parent_coordinate_system(self, parent_coordinate_system):
        self.parent_coordinate_system.set_value(parent_coordinate_system)

    def get_parent_coordinate_system(self):
        return self.parent_coordinate_system.get_value()

    def set_position(self, x, y, z):
        self.position.get_field("x").set_value(x)
        self.position.get_field("y").set_value(y)
        self.position.get_field("z").set_value(z)

    def get_position(self):
        return (
            self.position.get_field("x").get_value(),
            self.position.get_field("y").get_value(),
            self.position.get_field("z").get_value(),
        )

    def set_rotation(self, q1, q2, q3, q4):
        self.rotation.get_field("q1").set_value(q1)
        self.rotation.get_field("q2").set_value(q2)
        self.rotation.get_field("q3").set_value(q3)
        self.rotation.get_field("q4").set_value(q4)

    def get_rotation(self):
        return (
            self.rotation.get_field("q1").get_value(),
            self.rotation.get_field("q2").get_value(),
            self.rotation.get_field("q3").get_value(),
            self.rotation.get_field("q4").get_value(),
        )
