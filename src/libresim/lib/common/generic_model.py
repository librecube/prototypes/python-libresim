from libresim import (
    FallibleModel,
    EntryPointPublisher,
    Composite,
    Aggregate,
    EventConsumer,
    Container,
)


class GenericModel(
    FallibleModel, EntryPointPublisher, Composite, Aggregate, EventConsumer
):
    def __init__(self, name, description="", parent=None):
        FallibleModel.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)
        Composite.__init__(self, name, description, parent)
        Aggregate.__init__(self, name, description, parent)
        EventConsumer.__init__(self, name, description, parent)

        self.add_container(Container("ModelParameters"))

    def add_model_parameter(self, model_parameter):
        self.get_container("ModelParameters").add_component(model_parameter)

    def get_model_parameter(self, parameter_name):
        return self.get_container("ModelParameters").get_component(
            parameter_name
        )
