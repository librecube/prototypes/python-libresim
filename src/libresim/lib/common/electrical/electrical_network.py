from yaml import safe_load, YAMLError

from libresim import (
    Model,
    EntryPointPublisher,
    Composite,
    Container,
    EventConsumer,
    EventSink,
    EntryPoint,
)
from .switch import Switch
from .power_source import PowerSource
from .terminator import Terminator
from .node import Node


def create_electrical_network_from_file(
    filename,
    name="ElectricalNetwork",
    description="",
    parent=None,
):
    with open(filename) as file:
        try:
            config = safe_load(file)
        except YAMLError as error:
            print(error)
    electrical_network = ElectricalNetwork(name, description, parent)
    elements = electrical_network.get_container("Elements")

    def parse_data(key, value):
        for name, attributes in value.items():
            if attributes is None:
                attributes = {}
            if key == "power_sources":
                description = attributes.get("description", "")
                element = PowerSource(name, description, elements)
                element.voltage.set_value(attributes.get("voltage", 0))
                element.active.set_value(attributes.get("active", 0))
                elements.add_component(element)
            elif key == "nodes" or key == "terminators":
                description = attributes.get("description", "")
                if key == "nodes":
                    element = Node(name, description, elements)
                elif key == "terminators":
                    element = Terminator(name, description, elements)
                element.intrinsic_load.set_value(
                    attributes.get("intrinsic_load", 0)
                )
                element.low_voltage_threshold.set_value(
                    attributes.get("low_voltage_threshold", 0)
                )
                elements.add_component(element)
            elif key == "switches":
                description = attributes.get("description", "")
                element = Switch(name, description, elements)
                element.intrinsic_load.set_value(
                    attributes.get("intrinsic_load", 0)
                )
                element.position.set_value(attributes.get("position", 0))
                elements.add_component(element)
            elif key == "connect_elements":
                upper = elements.get_component(attributes.get("upper"))
                lower = elements.get_component(attributes.get("lower"))
                electrical_network.connect_elements(lower, upper)

    for key, value in config.items():
        parse_data(key, value)
    return electrical_network


class ElectricalNetwork(Model, EntryPointPublisher, Composite, EventConsumer):
    """An electrical network is a combination of elements with connections
    between them so that the elements build a top-down network.

    """

    def __init__(self, name="ElectricalNetwork", description="", parent=None):
        Model.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)
        Composite.__init__(self, name, description, parent)
        EventConsumer.__init__(self, name, description, parent)

        self.add_container(Container("Elements", "", self))

        self.add_entry_point(
            EntryPoint(
                "update",
                "Updates voltages and loads of all elements in the electrical network",
                self,
                self.do_update,
            )
        )

    def connect(self):
        self.get_scheduler().add_immediate_event(
            self.get_entry_point("update")
        )

    def add_element(self, element):
        self.get_container("Elements").add_component(element)
        for event_source in element.get_event_sources():
            event_sink = CommonEventSink(event_source.get_name(), parent=self)
            event_source.subscribe(event_sink)

    def get_element(self, name):
        return self.get_container("Elements").get_component(name)

    def validate(self):
        """Validates the complete electrical network. This includes calling
        the validate methods of the individual elements."""
        for element in self.get_container("Elements").get_components():
            if element.validate() is False:
                return False
        return True

    def connect_elements(self, lower, upper):
        if type(lower) == PowerSource:
            raise Exception
        if type(upper) == Terminator:
            raise Exception
        if lower not in self.get_container("Elements").get_components():
            self.add_element(lower)
        if upper not in self.get_container("Elements").get_components():
            self.add_element(upper)
        lower.get_reference("upper_elements").add_component(upper)
        upper.get_reference("lower_elements").add_component(lower)

    def do_update(self):
        for element in self.get_container("Elements").get_components():
            if type(element) == Terminator:
                element.determine_voltage()
        for element in self.get_container("Elements").get_components():
            # if type(element) == PowerSource:
            element.determine_load()


class CommonEventSink(EventSink):
    def notify(self, sender, args=None):
        print(sender.get_name())
        self.get_root().get_scheduler().add_immediate_event(
            self.get_parent().get_entry_point("update")
        )
