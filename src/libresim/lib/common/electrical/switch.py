from libresim import SimpleField

from .constants import PositionEnum, EventEnum
from .element import Element


def create_switch(
    name, description="", parent=None, intrinsic_load=None, position=None
):
    if None in [intrinsic_load, position]:
        raise ValueError
    switch = Switch(name, description, parent)
    switch.intrinsic_load.set_value(intrinsic_load)
    switch.position.set_value(position)
    return switch


class Switch(Element):
    def __init__(self, name, description="", parent=None):
        Element.__init__(self, name, description, parent)

        self.position = SimpleField("position", "Position of the switch", self)

    def publish(self):
        Element.publish(self)
        self._receiver.publish_field(self.position)

    def configure(self):
        Element.configure(self)
        self.low_voltage_threshold.set_value(0)

    def do_close(self):
        if self.position.get_value() != PositionEnum.CLOSED:
            self.position.set_value(PositionEnum.CLOSED)
            self.get_event_source(EventEnum.ON_CLOSED).emit()

    def do_open(self):
        if self.position.get_value() != PositionEnum.OPEN:
            self.position.set_value(PositionEnum.OPEN)
            self.get_event_source(EventEnum.ON_OPENED).emit()

    def is_closed(self):
        return self.position.get_value() == PositionEnum.CLOSED

    def is_open(self):
        return self.position.get_value() == PositionEnum.OPEN

    def determine_voltage(self):
        value = Element.determine_voltage(self)

        if self.position.get_value() == PositionEnum.OPEN:
            value = 0
        self.voltage.set_value(value)
        return self.voltage.get_value()

    def determine_load(self):
        value = 0
        lower_elements = self.get_reference("lower_elements")
        for lower_element in lower_elements.get_components():
            value += lower_element.determine_load()

        if value != 0:  # add intrinsic load only if downstream is some load
            value = value + self.intrinsic_load.get_value()

        if self.voltage.get_value() == 0:
            if self._is_powered:
                self._is_powered = False
                self.get_event_source(EventEnum.ON_NOT_POWERED).emit()
        else:
            if not self._is_powered:
                self._is_powered = True
                self.get_event_source(EventEnum.ON_POWERED).emit()

        self.load.set_value(value)
        return self.load.get_value()
