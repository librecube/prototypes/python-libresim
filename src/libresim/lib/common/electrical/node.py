from .element import Element


def create_node(
    name,
    description="",
    parent=None,
    intrinsic_load=None,
    low_voltage_threshold=None,
):
    if None in [intrinsic_load, low_voltage_threshold]:
        raise ValueError
    node = Node(name, description, parent)
    node.intrinsic_load.set_value(intrinsic_load)
    node.low_voltage_threshold.set_value(low_voltage_threshold)
    return node


class Node(Element):
    pass
