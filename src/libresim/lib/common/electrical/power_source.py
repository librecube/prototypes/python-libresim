from libresim import SimpleField

from .constants import PowerStatusEnum, EventEnum
from .element import Element


def create_power_source(
    name, description="", parent=None, voltage=None, active=None
):
    if None in [voltage, active]:
        raise ValueError
    power_source = PowerSource(name, description, parent)
    power_source.voltage.set_value(voltage)
    power_source.active.set_value(active)
    return power_source


class PowerSource(Element):
    def __init__(self, name, description="", parent=None):
        Element.__init__(self, name, description, parent)

        self.active = SimpleField("active", "Status of power source", self)

    def publish(self):
        self._receiver.publish_field(self.load)
        self._receiver.publish_field(self.voltage)
        self._receiver.publish_field(self.active)

    def validate(self):
        """Power source can have no elements above itself."""
        if len(self.get_reference("upper_elements").get_components()) != 0:
            return False
        return True

    def determine_voltage(self):
        if self.active.get_value() == PowerStatusEnum.POWERED:
            return self.voltage.get_value()
        else:
            return 0

    def determine_load(self):
        value = 0
        lower_elements = self.get_reference("lower_elements")
        for lower_element in lower_elements.get_components():
            value += lower_element.determine_load()
        self.load.set_value(value)
        return self.load.get_value()

    def do_power_on(self):
        if self.active.get_value() != PowerStatusEnum.POWERED:
            self.active.set_value(PowerStatusEnum.POWERED)
            self.get_event_source(EventEnum.ON_POWERED).emit()

    def do_power_off(self):
        if self.active.get_value() != PowerStatusEnum.NOT_POWERED:
            self.active.set_value(PowerStatusEnum.NOT_POWERED)
            self.get_event_source(EventEnum.ON_NOT_POWERED).emit()
