import enum


class PowerStatusEnum(enum.IntEnum):
    NOT_POWERED = 0
    POWERED = 1


class PositionEnum(enum.IntEnum):
    OPEN = 0
    CLOSED = 1


class EventEnum(enum.IntEnum):
    ON_LOAD_CHANGED = 1
    ON_POWERED = 2
    ON_NOT_POWERED = 3
    ON_FAILED = 4
    ON_UNFAILED = 5
    ON_OPENED = 6
    ON_CLOSED = 7
    ON_POSITION_CHANGED = 8
