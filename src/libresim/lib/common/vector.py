from libresim import SimpleField, StructureField


class Vector(StructureField):
    def __init__(self, name, description="", parent=None):
        StructureField.__init__(self, name, description, parent)

        self._fields = [
            SimpleField("x", "The x component of vector", self),
            SimpleField("y", "The y component of vector", self),
            SimpleField("z", "The z component of vector", self),
        ]
