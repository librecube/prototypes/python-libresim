from skyfield.framelib import ecliptic_frame
from skyfield.api import load, utc

from .celestial_body import CelestialBody


class Jupiter(CelestialBody):
    def __init__(self, name="Jupiter", description="", parent=None):
        CelestialBody.__init__(self, name, description, parent)

        self._jupiter_centric_ecliptical = None

        self._planets = load("de440s.bsp")
        self._planet = self._planets["jupiter barycenter"]
        self._ts = load.timescale()

    def set_jupiter_centric_ecliptical(self, component):
        self._jupiter_centric_ecliptical = component.get_name()
        self.get_reference("CoordinateSystems").add_component(component)
        component.parent_coordinate_system.set_value(None)

    def get_distance_to_sun(self):
        if self._jupiter_centric_ecliptical:
            cs = self.get_reference("CoordinateSystems").get_component(
                self._jupiter_centric_ecliptical
            )
            x = cs.position.x.get_value()
            y = cs.position.y.get_value()
            z = cs.position.z.get_value()
            distance = (x**2 + y**2 + z**2) ** (1 / 2)
            return distance

    def do_update(self):
        epoch = self.get_time_keeper().get_epoch_time()
        t = self._ts.utc(epoch.replace(tzinfo=utc))
        xyz = self._planet.at(t).frame_xyz(ecliptic_frame).m

        if self._jupiter_centric_ecliptical:
            cs = self.get_reference("CoordinateSystems").get_component(
                self._jupiter_centric_ecliptical
            )
            cs.position.x.set_value(xyz[0])
            cs.position.y.set_value(xyz[1])
            cs.position.z.set_value(xyz[2])
            cs.rotation.q1.set_value(0)
            cs.rotation.q2.set_value(0)
            cs.rotation.q3.set_value(0)
            cs.rotation.q4.set_value(0)

            self.distance_to_sun.set_value(self.get_distance_to_sun())
