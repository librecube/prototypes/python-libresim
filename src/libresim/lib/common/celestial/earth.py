import math
from datetime import timedelta

from skyfield.framelib import ecliptic_frame
from skyfield.api import load, utc
from scipy.spatial.transform import Rotation

from .celestial_body import CelestialBody


class Earth(CelestialBody):
    def __init__(self, name="Earth", description="", parent=None):
        CelestialBody.__init__(self, name, description, parent)

        self._earth_centric_ecliptical = None
        self._earth_centric_earth_equatorial = None
        self._earth_centric_earth_fixed = None

        self._planets = load("de440s.bsp")
        self._planet = self._planets["earth"]
        self._ts = load.timescale()

    def set_earth_centric_ecliptical(self, component):
        self._earth_centric_ecliptical = component.get_name()
        self.get_reference("CoordinateSystems").add_component(component)
        component.parent_coordinate_system.set_value(None)

    def set_earth_centric_earth_equatorial(self, component):
        self._earth_centric_earth_equatorial = component.get_name()
        self.get_reference("CoordinateSystems").add_component(component)
        component.parent_coordinate_system.set_value(
            self._earth_centric_ecliptical
        )

    def set_earth_centric_earth_fixed(self, component):
        if not self._earth_centric_ecliptical:
            raise ValueError
        self._earth_centric_earth_fixed = component.get_name()
        self.get_reference("CoordinateSystems").add_component(component)
        component.parent_coordinate_system.set_value(
            self._earth_centric_ecliptical
        )

    def get_distance_to_sun(self):
        if self._earth_centric_ecliptical:
            cs = self.get_reference("CoordinateSystems").get_component(
                self._earth_centric_ecliptical
            )
            x = cs.position.x.get_value()
            y = cs.position.y.get_value()
            z = cs.position.z.get_value()
            distance = (x**2 + y**2 + z**2) ** (1 / 2)
            return distance

    def do_update(self):
        epoch = self.get_time_keeper().get_epoch_time()
        t = self._ts.utc(epoch.replace(tzinfo=utc))
        xyz = self._planet.at(t).frame_xyz(ecliptic_frame).m

        if self._earth_centric_ecliptical:
            cs = self.get_reference("CoordinateSystems").get_component(
                self._earth_centric_ecliptical
            )
            cs.position.x.set_value(xyz[0])
            cs.position.y.set_value(xyz[1])
            cs.position.z.set_value(xyz[2])
            cs.rotation.q1.set_value(0)
            cs.rotation.q2.set_value(0)
            cs.rotation.q3.set_value(0)
            cs.rotation.q4.set_value(1)

            self.distance_to_sun.set_value(self.get_distance_to_sun())

        if self._earth_centric_earth_equatorial:
            cs = self.get_reference("CoordinateSystems").get_component(
                self._earth_centric_earth_equatorial
            )
            cs.position.x.set_value(0)
            cs.position.y.set_value(0)
            cs.position.z.set_value(0)
            cs.rotation.q1.set_value(-0.20312295319755547)
            cs.rotation.q2.set_value(0)
            cs.rotation.q3.set_value(0)
            cs.rotation.q4.set_value(0.9791532392247415)

        if self._earth_centric_earth_fixed:
            cs = self.get_reference("CoordinateSystems").get_component(
                self._earth_centric_earth_fixed
            )
            cs.position.x.set_value(0)
            cs.position.y.set_value(0)
            cs.position.z.set_value(0)

            sideral_degree = calculate_sideral_degree(epoch)
            q = Rotation.from_euler(
                "z", sideral_degree, degrees=True
            ).as_quat()
            cs.rotation.q1.set_value(q[0])
            cs.rotation.q2.set_value(q[1])
            cs.rotation.q3.set_value(q[2])
            cs.rotation.q4.set_value(q[3])


def calculate_sideral_degree(epoch):
    # Ref: Astronomical Algorithms, pg. 88
    jd = calculate_julian_date(epoch)
    T = (jd - 2451545) / 36525
    degrees = (
        280.46061837
        + 360.98564736629 * (jd - 2451545)
        + 0.000387933 * T**2
        - T**3 / 38710000
    )
    return degrees % 360


def calculate_julian_date(epoch):
    dt = timedelta(
        days=epoch.day,
        hours=epoch.hour,
        minutes=epoch.minute,
        seconds=epoch.second,
    )
    days = dt.total_seconds() / timedelta(days=1).total_seconds()
    return date_to_jd(epoch.year, epoch.month, days)


def date_to_jd(year, month, day):
    """
    Convert a date to Julian Day.

    Examples
    --------
    Convert 6 a.m., February 17, 1985 to Julian Day

    >>> date_to_jd(1985,2,17.25)
    2446113.75

    """
    if month == 1 or month == 2:
        yearp = year - 1
        monthp = month + 12
    else:
        yearp = year
        monthp = month

    # this checks where we are in relation to October 15, 1582, the beginning
    # of the Gregorian calendar.
    if (
        (year < 1582)
        or (year == 1582 and month < 10)
        or (year == 1582 and month == 10 and day < 15)
    ):
        # before start of Gregorian calendar
        B = 0
    else:
        # after start of Gregorian calendar
        A = math.trunc(yearp / 100.0)
        B = 2 - A + math.trunc(A / 4.0)

    if yearp < 0:
        C = math.trunc((365.25 * yearp) - 0.75)
    else:
        C = math.trunc(365.25 * yearp)

    D = math.trunc(30.6001 * (monthp + 1))
    jd = B + C + D + day + 1720994.5
    return jd
