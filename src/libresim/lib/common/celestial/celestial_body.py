from libresim import (
    Model,
    Aggregate,
    Reference,
    SimpleField,
    EntryPoint,
    EntryPointPublisher,
)


class CelestialBody(Model, Aggregate, EntryPointPublisher):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        Aggregate.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)

        self.distance_to_sun = SimpleField(
            "distance_to_sun", "Distance to Sun", self
        )

        self.add_reference(Reference("CoordinateSystems"))

        self.add_entry_point(
            EntryPoint(
                "update",
                "Update all associated coordinate systems",
                self,
                function=self.do_update,
            )
        )

    def publish(self):
        self._receiver.publish_field(self.distance_to_sun)

    def do_update(self):
        # to be implemented by child classes
        raise NotImplementedError
