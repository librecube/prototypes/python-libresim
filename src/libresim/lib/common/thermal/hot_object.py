from libresim import Model, SimpleField


def create_hot_object(name, description="", parent=None, status=None):
    if None in [status]:
        raise ValueError
    hot_object = HotObject(name, description, parent)
    hot_object.status.set_value(status)
    return hot_object


class HotObject(Model):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)

        self.status = SimpleField("status", "Status of the hot object", self)

    def publish(self):
        self._receiver.publish_field(self.status)
