from libresim import Model, SimpleField


def create_related_hot_object(
    name,
    description="",
    parent=None,
    hot_object_name=None,
    maximum_effect=None,
):
    if None in [hot_object_name, maximum_effect]:
        raise ValueError
    related_hot_object = RelatedHotObject(name, description, parent)
    related_hot_object.hot_object_name.set_value(hot_object_name)
    related_hot_object.maximum_effect.set_value(maximum_effect)
    return related_hot_object


class RelatedHotObject(Model):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        self.hot_object_name = SimpleField(
            "hot_object_name", "Name of the referenced hot object", self
        )
        self.maximum_effect = SimpleField(
            "maximum_effect", "Contribution (in degC) of the hot object", self
        )

    def publish(self):
        self._receiver.publish_field(self.hot_object_name)
        self._receiver.publish_field(self.maximum_effect)
