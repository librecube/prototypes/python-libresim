from yaml import safe_load, YAMLError

from libresim import (
    Model,
    Composite,
    SimpleField,
    EntryPoint,
    Container,
    EntryPointPublisher,
)
from .hot_object import HotObject
from .related_hot_object import RelatedHotObject
from .thermal_node import ThermalNode


def create_thermal_network(name, description="", parent=None, delta_time=None):
    if None in [delta_time]:
        raise ValueError
    thermal_network = ThermalNetwork(name, description, parent)
    thermal_network.delta_time.set_value(delta_time)
    return thermal_network


def create_thermal_network_from_file(
    filename,
    name="ThermalNetwork",
    description="",
    parent=None,
    delta_time=None,
):
    if None in [delta_time]:
        raise ValueError
    with open(filename) as file:
        try:
            config = safe_load(file)
        except YAMLError as error:
            print(error)

    thermal_network = ThermalNetwork(name, description, parent)
    thermal_network.delta_time.set_value(delta_time)

    hot_objects = thermal_network.get_container("HotObjects")
    thermal_nodes = thermal_network.get_container("ThermalNodes")

    def parse_data(key, value):
        for name, attributes in value.items():
            if attributes is None:
                attributes = {}

            if key == "hot_objects":
                description = attributes.get("description", "")
                ho = HotObject(name, description, hot_objects)
                status = attributes.get("status", 0)
                ho.status.set_value(status)
                hot_objects.add_component(ho)
            elif key == "thermal_nodes":
                description = attributes.get("description", "")
                tn = ThermalNode(name, description, thermal_nodes)
                tn.base_temperature.set_value(attributes["base_temperature"])
                tn.current_temperature.set_value(
                    attributes["current_temperature"]
                )
                tn.rise_rate.set_value(attributes["rise_rate"])
                tn.fall_rate.set_value(attributes["fall_rate"])
                thermal_nodes.add_component(tn)

                related_hot_objects = tn.get_container("RelatedHotObjects")

                if attributes.get("related_hot_objects"):
                    parse_related_hot_objects(
                        data=attributes["related_hot_objects"],
                        container=related_hot_objects,
                    )

    def parse_related_hot_objects(data, container):
        node_name = container.get_parent().get_name()
        for obj_name, max_effect in data.items():
            rho = RelatedHotObject(f"{node_name}_{obj_name}", "", container)
            rho.hot_object_name.set_value(
                thermal_network.get_hot_object(obj_name).get_name()
            )
            rho.maximum_effect.set_value(max_effect)
            container.add_component(rho)

    for key, value in config.items():
        parse_data(key, value)
    return thermal_network


class ThermalNetwork(Model, EntryPointPublisher, Composite):
    def __init__(self, name="ThermalNetwork", description="", parent=None):
        Model.__init__(self, name, description, parent)
        Composite.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)

        self.add_container(Container("HotObjects"))
        self.add_container(Container("ThermalNodes"))

        self.delta_time = SimpleField(
            "delta_time", "Delta time (in seconds) between two updates", self
        )

        self.add_entry_point(
            EntryPoint(
                "update",
                "Updates temperatures of all thermal nodes in the thermal network",
                self,
                self.do_update,
            )
        )
        self.update_id = None

    def configure(self):
        self.delta_time.set_value(1)

    def publish(self):
        self._receiver.publish_field(self.delta_time)

    def connect(self):
        if self.delta_time.get_value() > 0:
            self.update_id = self._scheduler.add_simulation_time_event(
                entry_point=self.get_entry_point("update"),
                simulation_time=0,
                cycle_time=self.delta_time.get_value(),
                repeat=-1,
            )

    def add_thermal_node(self, thermal_node):
        self.get_container("ThermalNodes").add_component(thermal_node)

    def add_hot_object(self, hot_object):
        self.get_container("HotObjects").add_component(hot_object)

    def do_change_delta_time(self, value):
        # remove event if it exists already
        if self.update_id is not None:
            self._scheduler.remove_event(self.update_id)
        # schedule update if delta time not zero
        self.delta_time.set_value(value)
        if self.delta_time.get_value() > 0:
            self.update_id = self._scheduler.add_simulation_time_event(
                entry_point=self.get_entry_point("update"),
                simulation_time=0,
                cycle_time=self.delta_time.get_value(),
                repeat=-1,
            )

    def get_hot_object(self, name):
        return self.get_container("HotObjects").get_component(name)

    def get_thermal_node(self, name):
        return self.get_container("ThermalNodes").get_component(name)

    def do_update(self):
        # updates all temperatures of all thermal nodes in the thermal network
        for tn in self.get_container("ThermalNodes").get_components():
            tn.do_update(self.delta_time.get_value())
