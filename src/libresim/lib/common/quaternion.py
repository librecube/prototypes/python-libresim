from libresim import SimpleField, StructureField


class Quaternion(StructureField):
    def __init__(self, name, description="", parent=None):
        StructureField.__init__(self, name, description, parent)

        self._fields = [
            SimpleField("q1", "The q1 component of quaternion", self),
            SimpleField("q2", "The q2 component of quaternion", self),
            SimpleField("q3", "The q3 component of quaternion", self),
            SimpleField("q4", "The q4 component of quaternion", self),
        ]
