import libresim


def get_dict_of_field_values(resolver, fields):
    """Iterates through the published fields and add them to a vector dict
    in the form of {absolute path: field value, ...}. If root model is an empty
    string, the published fields of the simulation will be added, otherwise
    only the fields belong to the root model and child models.
    """
    vector = {}

    for field in fields:
        path = resolver.get_absolute_path(field)
        if isinstance(field, libresim.SimpleField):
            vector[path] = field.get_value()
        elif isinstance(field, libresim.StructureField):
            vector.update(
                get_dict_of_field_values(resolver, field.get_fields())
            )
        else:
            raise NotImplementedError

    return vector


def print_simulator_tree(simulator):
    def traverse_model_tree(element, indent=0):
        print("  ╚" + indent * "═" + f" {element.get_name()}")
        if isinstance(element, libresim.Container):
            for component in element.get_components():
                traverse_model_tree(component, indent + 1)
        elif isinstance(element, libresim.Composite):
            for container in element.get_containers():
                print("  ╚" + (indent + 1) * "═" + f" {container.get_name()}")
                for component in container.get_components():
                    traverse_model_tree(component, indent + 2)

    traverse_model_tree(simulator)


def print_published_fields(simulator):
    for field in simulator.get_publication().get_fields():
        print("- {}".format(simulator.get_resolver().get_absolute_path(field)))
