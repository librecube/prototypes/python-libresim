import os
import sys
import argparse
import importlib
import threading
from xmlrpc.server import DocXMLRPCServer

import libresim


def server():
    # load model from external file
    parser = argparse.ArgumentParser()
    parser.add_argument("modelfile")
    args = parser.parse_args()

    # add current directory to path to be able to load modules
    sys.path.append(os.getcwd())

    # allow loading modules from subfolders
    modelfile = args.modelfile
    modelfile = modelfile.replace("/", ".")  # Linux
    modelfile = modelfile.replace("\\", ".")  # Windows
    # strip away file ennding if present
    if modelfile.endswith(".py"):
        modelfile = modelfile.strip(".py")

    module = importlib.import_module(modelfile)

    # create simulator
    simulator = libresim.Simulator()
    simulator.set_time_progress(libresim.SimulationTimeProgress.REALTIME)
    # simulator.set_time_progress(libresim.SimulationTimeProgress.ACCELERATED, 10)
    # simulator.set_time_progress(libresim.SimulationTimeProgress.FREE_RUNNING)

    # add models
    for component in module.root:
        if isinstance(component, libresim.Model):
            simulator.add_model(component)
        elif isinstance(component, libresim.Service):
            simulator.add_service(component)

    def traverse_model_tree(element, indent):
        print("  ╚" + indent * "═" + f" {element.get_name()}")
        if isinstance(element, libresim.Container):
            for component in element.get_components():
                traverse_model_tree(component, indent + 1)
        elif isinstance(element, libresim.Composite):
            for container in element.get_containers():
                print("  ╚" + (indent + 1) * "═" + f" {container.get_name()}")
                for component in container.get_components():
                    traverse_model_tree(component, indent + 2)

    print()
    print("Model tree:")
    traverse_model_tree(simulator.get_container("Models"), 0)
    print()

    def setup():
        simulator.publish()
        print("Published fields:")
        for field in simulator.get_publication().get_fields():
            print(
                f" * {field.get_parent().get_name()}.{field.get_name()}: {field.get_description()}"
            )
        print()
        simulator.configure()
        simulator.connect()

    rpc_server = DocXMLRPCServer(("0.0.0.0", 9999), allow_none=True)
    rpc_server.register_introspection_functions()
    rpc_server.register_function(lambda: "pong", "ping")

    rpc_server.register_function(setup, "setup")
    rpc_server.register_function(simulator.run, "run")
    rpc_server.register_function(simulator.hold, "hold")
    rpc_server.register_function(simulator.exit, "exit")

    thread = threading.Thread(target=rpc_server.serve_forever, daemon=True)
    thread.start()

    input("Press <Enter> to stop\n\n")
